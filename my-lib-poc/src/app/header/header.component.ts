import { HeaderService } from './header.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
 
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: ['h1 { font-weight: normal; }']
})
export class HeaderComponent implements OnInit {
  public pago:any=100;
  @Input() value:any=0;
  @Output() result = new EventEmitter();
public header: string='';
   constructor(private service: HeaderService) { }
  ngOnInit() {
    this.header = this.service.getHeader();
  }
  calcular(){
    let result= parseInt(this.value)+parseInt(this.pago);
    this.result.next(result);
  }
}