import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
 
  constructor() { }
 
  getHeader(): string {
    return 'Header service';
  }
 
}